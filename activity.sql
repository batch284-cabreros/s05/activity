-- 1. Return the customerName of the customers who are from the Philippines.

USE classic_models;

SELECT customerName FROM customers WHERE country = 'Philippines';


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".

USE classic_models;

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';


-- 3. Return the product name and MSRP of the product named "The Titanic".

USE classic_models;

SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';


-- 4. Return the first and last name of the employee whose email is "jfirreli@classicmodelcars.com".

USE classic_models;

SELECT firstName, lastName FROM employees WHERE email = 'jfirreli@classicmodelcars.com';


-- 5. Return the names of customers who have no registered state.

USE classic_models;

SELECT customerName FROM customers WHERE state IS NULL;



-- 6. Return the firsrt name, last name, email of the employee whose last name is Patterson and first name is Steve.

USE classic_models;

SELECT firstName, lastName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';



-- 7. Return customers name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.

USE classic_models;

SELECT customerName, country, creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;



-- 8. Return the customer numbers of orders whose comments contain the string''DHL'.

USE classic_models;

SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';



-- 9. Return the product lines whose text descriptions mentions the phrase 'state of the art'.

USE classic_models;

SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';


-- 10. Return the countries of customers without duplication.

USE classic_models;

SELECT DISTINCT country FROM customers;



-- 11. Return the statuses of orders without duplication.

USE classic_models;

SELECT DISTINCT status FROM orders;


-- 12. Return the customer names and countries of customers whose country is USA, France or Canada.

USE classic_models;

SELECT customerName, country FROM customers WHERE country IN ('USA', 'France', 'Canada');


-- 13. Return the first name, last name, and office's city of employees whose office are in Tokyo.

USE classic_models;

SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = 'Tokyo';


-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson".

USE classic_models;

SELECT customers.customerName FROM customers INNER JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Thompson';


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports".

USE classic_models;

SELECT products.productName, customers.customerName FROM products INNER JOIN orderdetails ON products.productCode = orderdetails.productCode INNER JOIN orders ON orderdetails.orderNumber = orders.orderNumber INNER JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerName = 'Baane Mini Imports';


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country.

USE classic_models;

SELECT e.firstName, e.lastName, c.customerName, o.country
FROM employees e
INNER JOIN customers c ON e.employeeNumber = c.salesRepEmployeeNumber
INNER JOIN orders odr ON c.customerNumber = odr.customerNumber
INNER JOIN offices o ON e.officeCode = o.officeCode
WHERE c.country = o.country;


-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.

USE classic_models;

SELECT productName, quantityInStock FROM products WHERE productLine = 'Planes' AND quantityInStock < 1000;



-- 18. Return the cutsomer's name with a phone number containing "+81".

USE classic_models;

SELECT customerName, phone FROM customers WHERE phone LIKE '%+81%';